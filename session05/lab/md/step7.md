#JSON

The format in which our Coffee is returned is something we made up, or rather made up using conventions og the Guava toString library. When attempting to communicate over a public protocol like http, a more robust format would be needed. Java Script Object Notation is one simple standard:

- <http://www.json.org/>

- <http://stackoverflow.com/questions/1695883/what-is-json-can-you-explain-it-to-a-newbie>

Producing and consuming JSON is painless in JavaScript, but can be tedious in Java. To ease the burden, we will use the Gson library

- <https://code.google.com/p/google-gson/>

Gson is already part of the play framework, so we do not need to download it.

Create a new package in your app folder called 'utils', and bring in the following class which encapsulates our usage of Gson:

~~~java
package utils;

import java.lang.reflect.Type;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import models.Coffee;

public class JsonParsers
{
  static Gson gson = new Gson();
  
  public static String user2Json(Object obj)
  {
    return gson.toJson(obj);
  }  
 
  public static Coffee json2Coffee(String json)
  {
    return gson.fromJson(json, Coffee.class);    
  }
  
  public static String coffee2Json(Object obj)
  {
    return gson.toJson(obj);
  }  
  
  public static List<Coffee> json2Coffees(String json)
  {
    Type collectionType = new TypeToken<List<Coffee>>() {}.getType();
    return gson.fromJson(json, collectionType);    
  }  
}
~~~

This should compile without errors.


The two methods this class provides will enable us to easily transform a Coffee object to/from JSON format.

Try it out now - here is another version of the CoffeeService.coffee method:

~~~java
  public static void coffee (Long id)
  {
   Coffee coffee = Coffee.findById(id);
   renderJSON (JsonParsers.coffee2Json(coffee));
  }
~~~

You will need to import JsonParsers at the top of the class:

~~~
import utils.JsonParsers;
~~~

Now try browsing here:

- <http://localhost:9000/api/coffee/1>

This should show:

![](../img/04.png)

It differs only slightly - but whereas previously he had an ad-hoc format for serializing the object, this version adheres strictly to JSON conventions.

We need one more class in utils, which you can introduce now:

~~~java
package utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import play.data.binding.Global;
import play.data.binding.TypeBinder;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

@Global
public class GsonBinder implements TypeBinder<JsonElement>
{
  public Object bind(String name, Annotation[] notes, String value, Class toClass, Type toType) throws Exception
  {
    return new JsonParser().parse(value);
  }
}
~~~

The significance of this will be apparent later.

