#New Route

Next we introduce a new route to read a coffee object:

~~~
GET     /api/coffees                           CoffeeServiceAPI.getCoffees
~~~

Place this in the routes file - towards the top.

This is the corresponding controller:

~~~java
public class CoffeeServiceAPI extends Controller
{
  public static void getCoffees()
  {
    List<Coffee> coffee = Coffee.findAll();
    renderText(coffee.size());
  }
}
~~~

You may also wish to enable the default 'in memory' database at this stage in application.conf:

~~~
db=mem
~~~

Run the app now, and see what you get when you browse to:

- <http://localhost:9000/api/coffees>

You may see something like this:

![](../img/00.png)