#Test Data

To seed our application with some test data - we can bring in bootstrap.java into the app folder:

~~~java
import java.util.List;

import play.*;
import play.jobs.*;
import play.test.*;
 
import models.*;
 
@OnApplicationStart
public class Bootstrap extends Job 
{ 
  public void doJob()
  {
    Fixtures.loadModels("data.yml");
  }
}
~~~

.. and some test data in the conf/data.yml:

~~~
Coffee(c1):
    name      : one
    shop      : here
    price     : 2.0
    rating    : 3.5
    favourite : 0

Coffee(c2):
    name      : two
    shop      : there
    price     : 3.0
    rating    : 4.5
    favourite : 1
~~~

This procedure should be familiar. Launch the app again and browse to:

- <http://localhost:9000/api/coffees>

This time we should see the number 2:

![](../img/01.png)

Browse the database as well and verify that the two coffes'e have been loaded:

- <http://localhost:9000/@db>

