#Parameters

Introduce a new method into the controller to retrieve a specific Coffee object based on an id:

~~~java
  public static void coffee (Long id)
  {
   Coffee coffee = Coffee.findById(id);
   renderText (coffee);
  }
~~~

... and a matching route:

~~~
GET     /api/coffee/{id}                       CoffeeServiceAPI.coffee
~~~

You have seen this type of route before - id will be replaced by a number when the route is triggered.

Try it now (restart the app first):

- <http://localhost:9000/api/coffee/1>

This should show:

![A Test Figure](../img/03.png)

Try reading the object with id 2, or 0, or 3.

Add some more coffees to the data.yml file. Verify that they can be read.

