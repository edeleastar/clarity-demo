#Exercises

This is an archive of the lab so far:

- [coffeemate-service-lab-05](../archives/coffeemate-service-lab05.zip)


##Trace Request

Todays lectures showed a trace sequence on Spacebook. Using the same tools, see if you can trace:

- <http://localhost:9000/api/coffee/1>

You should be aiming to see something like this:

![](../img/05.png)

##Postman

Visit the Chrome Web Store, and search for the "Postman Rest Client" application:

![](../img/06.png)

Install it (the first one above) and explore how to send the same request using this tools. See if you can figure out the various options


##Coffee List

Introduce a new route which can support the following request:

- <http://localhost:9000/api/coffees>

It should return a list of all of the coffees in the database.