#Initial Version of Play Project

Create a new play project in the usual way - call this project 'coffeemate-service'.

Make sure you can launch the project in the usual way, and also that you can run the tests and browse the database (which will be empty).

Introduce the following model class:

~~~java
package models;

import java.io.Serializable;
import javax.persistence.*;
import play.db.jpa.Model;

@Entity
public class Coffee extends Model
{
  public String name;
  public String shop;
  public double rating;
  public double price;
  public int    favourite;

  public Coffee(String coffeeName, String shop, double rating, double price, int favourite)
  {
    this.name      = coffeeName;
    this.shop      = shop;
    this.rating    = rating;
    this.price     = price;
    this.favourite = favourite;
  }
}
~~~

... and the following test to verify that the model is operating correctly:

~~~java
import org.junit.*;
import java.util.*;
import play.test.*;
import models.*;

public class CoffeeTest extends UnitTest
{
  @Test
  public void testCreate()
  {
    Coffee coffee = new Coffee ("one", "here", 3.5, 2.0, 0);
    coffee.save();
    Coffee one = Coffee.findById(coffee.id);
    assertEquals ("one", one.name);
  }
}
~~~

Run the app in test mode, and make sure the test passes.