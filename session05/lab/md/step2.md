#Guava Library equals method

Often, the JDK libraries can be augmented with various extensions and utilities. Guava is one of the most prominent:

- <https://code.google.com/p/guava-libraries/wiki/GuavaExplained>

as it contains a range of useful classes. To use Guava in play, open the conf/dependencies.yml file:

~~~
# Application dependencies

require:
    - play
~~~

and add on a single entry at the end:

~~~
    - com.google.guava -> guava 14.0.1   
~~~

make sure you line it up precisely - 4 leading spaces required. For this to be reflected in your project you need to do two things:

- enter 'play deps' into the command line inside your project
- enter 'play eclipsify' likewise
- refresh the project in eclipse

It will not cause any noticeable change in the project, although you should see some new jar files in the 'lib' folder.

We will use guava methods sparingly - but one useful one right away. Open Coffee.java, and import this class with the other imports:

~~~java
import com.google.common.base.Objects;
~~~

Then include this new method on Coffee:

~~~java
  @Override
  public boolean equals(final Object obj)
  {
    if (obj instanceof Coffee)
    {
      final Coffee other = (Coffee) obj;
      return Objects.equal(name,      other.name) 
          && Objects.equal(shop,      other.shop)
          && Objects.equal(rating,    other.rating)
          && Objects.equal(price,     other.price)           
          && Objects.equal(favourite, other.favourite);                     
    }
    else
    {
      return false;
    }
  } 
~~~

We have equipped our Model class with a useful 'equals' method - which will allow us to easily compare model Coffee objects. This will be particularly useful when writing tests.

Here is a rework of the singe test we have written:

~~~java
  @Test
  public void testCreate()
  {
    Coffee coffee = new Coffee ("one", "here", 3.5, 2.0, 0);
    coffee.save();
    Coffee one = Coffee.findById(coffee.id);
    assertEquals (coffee, one);
  }
~~~

The only change is the last line - which now does a full compare of the two coffee objects.