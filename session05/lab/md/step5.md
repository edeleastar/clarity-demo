#toString Method

Back in Coffee, we can introduce a method, often included as a companion to equals(), called toString():

~~~java
  public String toString()
  {
    return Objects.toStringHelper(this)
        .add("Id", id)
        .add("name",      name)
        .add("shop",      shop)
        .add("rating",    rating)
        .add("price",     price)
        .add("favourite", favourite).toString();
  } 
~~~

This method will return a readable string version of any Coffee object. Useful for logging, testing or other purposes.

We are going to use it immediately to turn a Coffee object into a string, and send it in this form to a browser. Change the CoffeeService controller as follows:

~~~java
public class CoffeeServiceAPI extends Controller
{
  public static void getCoffees()
  {
    List<Coffee> coffees = Coffee.findAll();
    renderText(coffees.get(0).toString());
  }
}
~~~

Here we are returning the first coffee object in the database. Try browsing to 

- <http://localhost:9000/api/coffees>

and we should see:

![](../img/02.png)

Please note you will almost certainly have to restart the application for this to work correctly. Inspect the source of the page. Can you see what is going on?

Back in the controller, we can simplify the renderText call:

~~~java
    renderText(coffees.get(0));
~~~

As this method expects a String, toString will be automatically called whenever an object is passed.
