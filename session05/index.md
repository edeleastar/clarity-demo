
- Explore the http protocol in more depth
- Understand the difference between GET and POST http requests
- Be able to trace a http request/response cycle