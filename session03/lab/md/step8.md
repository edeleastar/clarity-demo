#Manipulating Repository using Sourcetree

##Loading the repository into Sourcetree

The repository - which contains all versions of your project, can be inspected in any tool that understands git. Open the Souretree tool you just downloaded.

![](../img/23.png)

In Explorer/Finder, drag and drop your project folder (schema) into the left hand pane. The project should now be browsable as follows:

![](../img/24.png)

Take some time to explore each of the 'commits'. In particular, examine the how you can explore each revision, each file and the changes made to each file. These changes are shown visually in the bottom right (depending on which commit/file you select).

##Pushing the repository to bitbucket.

Create a free account on this service here:

-<http://bitbucket.org/>

Once logged in, create a new Repository, and call it schema:

![](../img/25.png)

This should lead you to this screen:

![](../img/26.png)

On the top right, select 'clone' - and in the drop down select 'HTTPS' as shown here:

![](../img/27.png)

We are interested in extracting the path form the text field. It may look something like this:

~~~
https://edel020@bitbucket.org/edel020/schema.git
~~~

(on your account, edel020 will be your bitbucket username).

Copy this string to the clipboard. Back in Sourcetree, right click on 'Remotes' and select "New Remote":

![](../img/28.png)

Select 'Add'

![](../img/29.png)

and paste in the url we copied from bitbucket. Now select master and press 'ok'

![](../img/30.png)

Your local repository is now associated with the repository you created on bitbucket. The next step is to 'push' your local change to the remote copy. Press the 'Push' button. You will be asked for your password in this step. After a few minutes your repository will be replicated on the bitbucket servers.

Browse the project on bitbucket:

![](../img/31.png)

In particular, have a look at the "Commits" view. It provides a more comprehensive view of all of the changes associated with a particular commit.



