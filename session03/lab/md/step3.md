#Commit Baseline Schema project to git repository

Create a new file in your project folder called '.gitignore'. The file should contain the following:

~~~
# Ignore all dotfiles...
.*
# except for .gitignore
!.gitignore
!.classpath
!.project


# Ignore Play! working directory #
war
db
eclipse
lib
log
logs
modules
precompiled
project/project
project/target
crud/*
data/*
conf/crud/*
conf/cloudbees*
public/crud/*
target
tmp
test-result
server.pid
*.iml
*.eml
~~~

You may need to use notepad to create this file.

In eclipse, select the schema project in project explorer, right click and select 'Team->Share Project'.

![](../img/07.png)

![](../img/08.png)

Select Git and press Next:

![](../img/09.png)

Select 'Use or create project in parent folder of project'

![](../img/10.png)

Then highlight the project location, and press 'Create Repository'.

If all goes according to plan, your project will look like this:

![](../img/12.png)

The workspace looks the same, except many of the artifacts have a question mark associated with them. This means that, although the project is under revision ('git') control, the annotated artifacts are no yet under control.

To place all of these projects into git, right click on the project and select "Team->Commit":

![](../img/13.png)

Make sure all files are selected, and enter the comit message as shown above. Press 'Commit',  and the project will now look slightly different:

![](../img/14.png)

These revised icons indicate that the files are now under git control.



