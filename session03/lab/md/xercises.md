#Exercises

###Check out experiments

Check out and execute each version of the schema app, one at a time. Verify that each version is as expected

###Check out to another folder

Using sourcetree, see if you can check out earlier versions of the app, to different folders. See if you can have several versions of the project, side-by-side, on your pc.


##Create another repo

See if you can create a repository for a standard java application - perhaps your current programming assignment (as opposed to a Play app). Explore the repository both in eclipse and sourcetree.


##Share your repo

Get the bitbucket name of one of your classmates. Find out how to share one of your repos with them.

