#Create Schema Project

Locate your current workspace folder and create a new play project called 'schema':

~~~
play new schema
~~~

Change into the newly created project, and eclipsify it:

~~~
cd schema
play eclipsify
~~~

Now import into eclipse - your project should look like this:

![](../img/04.png)

Run the project:

~~~
play run
~~~

Browsing to local host:

- <http://localhost:9000>

![](../img/05.png)


Change the template views/Application/index.html

~~~html
#{extends 'main.html' /}
#{set title:'Home' /}

<p> Hello From Play! </p>
~~~

and refresh the page:

![](../img/06.png)

