#Install Git, Sourcetree & Egit

##Git

Download and install git on your pc:

- <http://git-scm.com/downloads>

Accept the default settings.

##Sourcetree

When this has completed, install sourcetree on your PC

- <http://www.atlassian.com/software/sourcetree/overview>

Again, accept the default settings. During the install you will be asked to enter your full name + email.

##Egit

Finally, install egit - an extension for Eclipse. To do this:

- In Eclipse select Window->Install new Software:

![](../img/01.png)

Press 'Add' and paste the following : "Egit" and "http://download.eclipse.org/egit/updates" into the dialog as shown:

![](../img/02.png)

Press 'Ok' and select all components:

![](../img/03.png)

Press 'Next' and proceed through install, accepting license agreement on the way. Eclipse will need to restart for the changes to take effect.