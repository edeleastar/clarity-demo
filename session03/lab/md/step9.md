#Commit Bidirectional Player/Club

We will establish a relationship from player to club - making the existing association bi-directional

##Club:

Change the OneToMany mapping in Club

~~~java
   @OneToMany(mappedBy="club", cascade=CascadeType.ALL)
   public List<Player> players;
~~~

and also the addPlayer method:

~~~java
   public void addPlayer(Player player)
   {
     player.club = this;
     players.add(player);
   }
~~~

##Player

Introduce this import:

~~~java
import javax.persistence.ManyToOne;
~~~

..and this attribute:

~~~java
  @ManyToOne
  public Club club;
~~~

##ClubTest

and a new test to verify the relationship:

~~~java
  @Test
  public void testPlayerClub()
  {
    Player mike  = Player.findByName("mike");
    assertNotNull (mike.club);
    assertEquals ("tramore", mike.club.name);
  }
~~~

Run the tests to make sure everything is as expected.

Remaining in eclipse, commit the changes we have just made with the commit message 'Bidirectional Club/Player association'.

Still in Eclipse, select "Team->Push to Upstream" (you will be asked for your password). This will push the most recent commit to your bitbucket repository. Browse to the bitbucket repo now and verify that the commit has worked. In particular look for the most recent commit message + changes.

It should look something like this:

![](../img/32.png)

