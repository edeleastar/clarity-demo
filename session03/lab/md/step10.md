#Checkout Earlier Revision

Reverting to an earlier revision - without loosing later versions - is one of the key benefits of using git. We will try now to revert to a version of the project before we introduce the relationship between Club and Player. Locate the 'Repositories' view in eclipse:

![](../img/33.png)

We have 5 versions of the app in the repo. Note each of them has an id, message, author, date etc...

Suppose we want to revert to the previous version - the one with the message 'Club/Player one-to-many relationship with tests'. Before we do so, open the Model/Player class in the editor - and verify that the following line is in the Player class:

~~~
  @ManyToOne
  public Club club;
~~~

In the "History" view, right click on the previous version, and select 'Checkout'

![](../img/34.png)

You should notice that the player class now looks like this:

~~~
package models;

import javax.persistence.Entity;
import play.db.jpa.Model;

@Entity
public class Player extends Model
{
  public String name;

  public Player(String name)
  {
    this.name = name;
  }
 
  public String toString()
  {
    return name;
  }
  
  public static Player findByName(String name)
  {
    return find("name", name).first();
  }
}
~~~

Also notice that the 'HEAD' tag in history is associated with the previous version:

![](../img/35.png)

Verify that the tests are also testing the previous version.

Our very last set of changes (Bidirectional Club/Player) have not been lost however. Right click on that version in history, and 'Checkout' again, will see those changes reintroduced.

~~~
package models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import play.db.jpa.Model;

@Entity
public class Player extends Model
{
  public String name;
  
  @ManyToOne
  public Club club;
  
  public Player(String name)
  {
    this.name = name;
  }
 
  public String toString()
  {
    return name;
  }
  
  public static Player findByName(String name)
  {
    return find("name", name).first();
  }
}
~~~
