#Commit Simple Player & Club Tests

Extend the Player class with this utility method:

~~~java
  public static Player findByName(String name)
  {
    return find("name", name).first();
  }
~~~

and this in club:

~~~java
  public static Club findByName(String name)
  {
    return find("name", name).first();
  } 
~~~

We can now replace the skeleton (empty) tests with these simple tests:

##PlayerTest

~~~java
import org.junit.*;

import java.util.*;
import play.test.*;
import models.*;

public class PlayerTest extends UnitTest 
{
  private Player p1, p2, p3;
  
  @Before
  public void setup()
  {
    p1 = new Player("mike");
    p2 = new Player("jim");
    p3 = new Player("frank");
    p1.save();
    p2.save();
    p3.save();
  }
  
  @After
  public void teardown()
  {
    p1.delete();
    p2.delete();
    p3.delete();
  }

  @Test
  public void testCreate()
  {
    Player a = Player.findByName("mike");
    assertNotNull(a);
    assertEquals("mike", a.name);
    Player b = Player.findByName("jim");
    assertNotNull(b);
    assertEquals("jim", b.name);
    Player c = Player.findByName("frank");
    assertNotNull(c);
    assertEquals("frank", c.name);
  }
  
  @Test
  public void testNotThere()
  {
    Player a = Player.findByName("george");
    assertNull(a);
  }
  
}
~~~

##ClubTest

~~~java
import org.junit.*;

import java.util.*;
import play.test.*;
import models.*;

public class ClubTest extends UnitTest 
{
  private Club c1, c2, c3;
  
  @Before
  public void setup()
  {
    c1 = new Club("tramore");
    c2 = new Club("dunmore");
    c3 = new Club("fenor");
    c1.save();
    c2.save();
    c3.save();
  }
  
  @After
  public void teardown()
  {
    c1.delete();
    c2.delete();
    c3.delete();
  }

  @Test
  public void testCreate()
  {
    Club a = Club.findByName("tramore");
    assertNotNull(a);
    assertEquals("tramore", a.name);
    Club b = Club.findByName("dunmore");
    assertNotNull(b);
    assertEquals("dunmore", b.name);
    Club c = Club.findByName("fenor");
    assertNotNull(c);
    assertEquals("fenor", c.name);
  }
  
  @Test
  public void testNotThere()
  {
    Club a = Club.findByName("george");
    assertNull(a);
  }
}
~~~

Bring these into the project and verify that the tests pass. Note the status of project, particularly this icons indicating which files have been changed.

This is a reasonably significant feature - so will commit it to git. Right click on the project, select 'Team->Commit':

![](../img/19.png)

This time we are not adding any new files, just changes to existing files. Try to notice the indicators in the project changing after the commit.

We now have three 'commits' in the history view:

![](../img/20.png)




