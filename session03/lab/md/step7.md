#Commit Club/Player One to Many Relationship

Introduce a one-to-many relationship between club and player.

First, introduce the following imports into Club:

~~~
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
~~~

Then extend the Club class with this new attribute:

~~~java
  @OneToMany(cascade=CascadeType.ALL)
  public List<Player> players;
~~~

Make sure the constructor creates this attribute:

~~~java
  public Club(String name)
  {
    this.name = name;
    this.players = new ArrayList<Player>();
  }
~~~

... and this method:

~~~java
  public void addPlayer(Player player)
  {
    players.add(player);
  }
~~~

To verify the relationship, we introduce some new fixture into ClubTest:

~~~java
  private Player p1, p2, p3;
~~~

rework the setup method to create these objects:

~~~java
  @Before
  public void setup()
  {
    p1 = new Player("mike");
    p2 = new Player("jim");
    p3 = new Player("frank");
    c1 = new Club("tramore");
    c2 = new Club("dunmore");
    c3 = new Club("fenor");
    
    c1.addPlayer(p1);
    c1.addPlayer(p2);
    
    c1.save();
    c2.save();
    c3.save();
  }
~~~

and we bring in these new tests:

~~~java
  @Test
  public void testPlayers()
  {
    Club tramore = Club.findByName("tramore");
    
    assertEquals (2, tramore.players.size());
    
    Player mike  = Player.findByName("mike");
    Player jim   = Player.findByName("jim");
    Player frank = Player.findByName("framk");
    
    assertTrue  (tramore.players.contains(mike));
    assertTrue  (tramore.players.contains(jim));
    assertFalse (tramore.players.contains(frank));
  }  
  
  @Test
  public void testRemovePlayer()
  {
    Club tramore = Club.findByName("tramore");
    assertEquals(2, tramore.players.size());
    
    Player mike  = Player.findByName("mike");
    assertTrue(tramore.players.contains(mike));
    tramore.players.remove(mike);
    tramore.save();
    
    Club c = Club.findByName("tramore");
    assertEquals(1, c.players.size());
    
    mike.delete();
  } 
~~~

Save everything, and verify that the tests all pass.

Now that the tests pass, commit the modifications to git. See if you can visual locate the (2) changed files first, and then commit them with the commit message "Club/Player one-to-many relationship with tests"

Your history may now look something like this:

![](../img/21.png)


