#Create 'Walking Skeleton' Version

We will now rebuild the project from Lab09, except this time we will commit each modification to git. Furthermore, we will commit these changes in stages, with suitable commit messages. In a later step we can inspect these messages and even revert to earlier versions if we wish.

## Models

Create these two model classes in models:

###Player

~~~java
package models;

import javax.persistence.Entity;
import play.db.jpa.Model;

@Entity
public class Player extends Model
{
  public String name;

  public Player(String name)
  {
    this.name = name;
  }
 
  public String toString()
  {
    return name;
  }
}
~~~

###Club

~~~java
package models;

import javax.persistence.Entity;
import play.db.jpa.Model;

@Entity
public class Club extends Model
{
  public String name;

  public Club(String name)
  {
    this.name = name;
  }
  
  public String toString()
  {
    return name;
  }
}
~~~

## Skeleton tests

Bring in these (empty) test cases into test

###PlayerTest

~~~java
import org.junit.*;

import java.util.*;
import play.test.*;
import models.*;

public class PlayerTest extends UnitTest 
{
  @Before
  public void setup()
  {
  }
  
  @After
  public void teardown()
  {
  }

  @Test
  public void testCreate()
  {
    
  }
}
~~~

###ClubTest

~~~java
import org.junit.*;

import java.util.*;
import play.test.*;
import models.*;

public class ClubTest extends UnitTest 
{
  @Before
  public void setup()
  {
  }
  
  @After
  public void teardown()
  {
  }

  @Test
  public void testCreate()
  {
    
  }
}
~~~



Run the project in test mode:

~~~
play test
~~~

and verify that the test and admin consoles appear:

- <http://localhost:9000/@tests>
- <http://localhost:9000/@db>

This complete the 'walking skeleton' phase. 



