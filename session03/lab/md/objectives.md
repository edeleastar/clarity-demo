#Objectives

- Explore the primary features of a git repository
- Set up a repository for a Play project in eclipse
- Progressively introduce and commit new features
- Review the repository in Sourcetree
- Push the repository to bitbucket - a cloud provider of git services.