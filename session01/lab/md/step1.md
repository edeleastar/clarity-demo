#Simple Test Harness

You have seen until testing in Eclipse for simple Java applications. There is an equivalent for the Play Framework. Unit testing is particularly important if you make any changes to the model, as it allows you to verify if the model is defined correctly.

Using your own spacebook project - or this solution to the second assignment from last semester:

- [spacebook-assign-2](../archives/spacebook-assign-2.zip)

In eclipse, notice that you already have a 'test' folder, with some tests already in there:

![caption](../img/01.png)

![caption one](../img/01.png "Figure 2")

Open the file 'BasicTest.java':

~~~java
import org.junit.*;
import java.util.*;
import play.test.*;
import models.*;

public class BasicTest extends UnitTest
{
  @Test
  public void aVeryImportantThingToTest()
  {
    assertEquals(2, 1 + 1);
  }
}
~~~

Executing this is a little different from executing standard junit tests, and requires the following stages:

## Stage 1: Run the spacebook app in 'test mode'

From the console, within the spacebook folder, run the app ass follows:

~~~
play test
~~~

This will respond as follows:

~~~
~        _            _ 
~  _ __ | | __ _ _  _| |
~ | '_ \| |/ _' | || |_|
~ |  __/|_|\____|\__ (_)
~ |_|            |__/   
~
~ play! 1.2.5, http://www.playframework.org
~ framework ID is test
~
~ Running in test mode
~ Ctrl+C to stop
~ 
CompilerOracle: exclude jregex/Pretokenizer.next
Listening for transport dt_socket at address: 8000
08:31:32,518 INFO  ~ Starting /Users/edeleastar/repos/modules/app-dev-modelling-prj/spacebook-3
08:31:32,522 INFO  ~ Module cloudbees is available (/Users/edeleastar/repos/modules/app-dev-modelling-prj/spacebook-3/modules/cloudbees-0.2.2)
08:31:32,523 INFO  ~ Module crud is available (/Users/edeleastar/dev/play-1.2.5/modules/crud)
08:31:33,136 WARN  ~ You're running Play! in DEV mode
~
~ Go to http://localhost:9000/@tests to run the tests
~
08:31:33,242 INFO  ~ Listening for HTTP on port 9000 (Waiting a first request to start) ...
~~~

## Stage 2: Browse to the 'test runner'

In chrome, bring up a browser are the following url:

- <http://localhost:9000/@tests>

This should show this:

!["caption"](../img/02.png)

Be careful here - we only want to select the 'BasicTest' - so click once on it, and the page will look like this:

![](../img/03.png)

## Stage 3: Run a successful Test

Now press 'Start' and note the changes in the page:

![](../img/04.png)

This has run the tests - but as the trivial test passed, then the page remained green.

## Stage 4: Run a failing Test

Make a small change to the test such that it is guaranteed to fail:

~~~java
  public void aVeryImportantThingToTest()
  {
    assertEquals(3, 1 + 1);
  }
~~~

Now run it again:


![](../img/05.png)

Note carefully the error report.





