#Users Test

Lets write some test for the classes we already have working. Before we do so, there is an occasional anomaly in play that can cause some confusion. So, open 'app/default package/BootStrap.java' and comment out the following:

~~~java
import java.util.List;

import play.*;
import play.jobs.*;
import play.test.*;
 
import models.*;
 
@OnApplicationStart
public class Bootstrap extends Job 
{ 
  public void doJob()
  {
    //Fixtures.deleteDatabase();
    //Fixtures.loadModels("data.yml");
  }
}
~~~

We are removing the objects loaded from the yaml file for the moment. Remember to put this back in when you are finished testing.

Now introduce the following test into the BasicTest class:

~~~java
public class BasicTest extends UnitTest
{
  @Test
  public void aVeryImportantThingToTest()
  {
    assertEquals(2, 1 + 1);
  }
  
  @Test
  public void testCreateUser()
  {
    User bob = new User("bob", "jones", 20, "irish", "bob@jones.com", "secret");
    bob.save();
    
    User testUser = User.findByEmail("bob@jones.com");
    assertNotNull (testUser);
  }
}
~~~

Look carefully at the 'testCreateUser()' test. In it we are creating a new user, saving it, and then seeing if we can find the user by the email address. Save and run this test.

It should succeed. Now, make a deliberate mistake in the test - try find 'alice' instead of 'bob' by making the following adjustment:

~~~java
  @Test
  public void testCreateUser()
  {
    User bob = new User("bob", "jones", 20, "irish", "bob@jones.com", "secret");
    bob.save();
    
    User testUser = User.findByEmail("alice@jones.com");
    assertNotNull (testUser);
  }
~~~

Run tests again, you should see something like this:

![](../img/07.png)

Change it back to 'bob' and confirm that the error is now gone.






