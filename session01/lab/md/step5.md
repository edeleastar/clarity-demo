#Class Specific Tests

Our complete test case now looks like this:

~~~java
import org.junit.*;

import java.util.*;

import play.test.*;
import models.*;

public class BasicTest extends UnitTest
{
  
  @BeforeClass
  public static void loadDB()
  {
    Fixtures.deleteAllModels();
  }
  
  @Test
  public void testCreateUser()
  {
    User bob = new User("bob", "jones", 20, "irish", "bob@jones.com", "secret");
    bob.save();
    
    User testUser = User.findByEmail("bob@jones.com");
    assertNotNull (testUser);
  }
  
  @Test
  public void testFindUser()
  {
    User jim = new User("jim", "smith", 20, "irish", "jim@smith.com", "secret");
    jim.save();
    
    User test = User.findByEmail("jim@smith.com");
    assertNotNull (test);

    User alice = User.findByEmail("alice@jones.com");
    assertNull (alice);
  }  
  
  @Test
  public void testCreateMessage()
  {
    User mary = new User("mary", "colllins", 20, "irish", "mary@collins.com", "secret");
    mary.save();
    
    User joan = new User("joan", "colllins", 20, "irish", "joan@collins.com", "secret");
    joan.save();
    
    Message msg = new Message (mary, joan, "Hi there - how are you");
    msg.save();
    
    List<Message> joansMessages = Message.find("byTo", joan).fetch();
    assertEquals (joansMessages.size(), 1);
  }
  
  @Test
  public void testNoMessagese()
  {
    User mary = new User("mary", "colllins", 20, "irish", "mary@collins.com", "secret");
    mary.save();
    
    List<Message> joansMessages = Message.find("byTo", mary).fetch();
    assertEquals (joansMessages.size(), 0);
  }
  
  @Test
  public void testMultipleMessages()
  {
    User mary = new User("mary", "colllins", 20, "irish", "mary@collins.com", "secret");
    mary.save();
    
    User joan = new User("joan", "colllins", 20, "irish", "joan@collins.com", "secret");
    joan.save();
    
    Message msg1 = new Message (mary, joan, "Hi there - how are you");
    msg1.save();
    Message msg2 = new Message (mary, joan, "Where are you now?");
    msg2.save();
    
    List<Message> joansMessages = Message.find("byTo", joan).fetch();
    assertEquals (joansMessages.size(), 2);
    Message message1 = joansMessages.get(0);
    assertEquals(message1.messageText, "Hi there - how are you");
    Message message2 = joansMessages.get(1);
    assertEquals(message2.messageText, "Where are you now?");   
  }
}
~~~

This mixes in User and Message tests. We can factor these out into classes of their own. In the same package, create a class call UserTest containing the following:

~~~java
import org.junit.*;

import java.util.*;

import play.test.*;
import models.*;

public class UserTest extends UnitTest
{
  
  @BeforeClass
  public static void loadDB()
  {
    Fixtures.deleteAllModels();
  }
  
  @Test
  public void testCreateUser()
  {
    User bob = new User("bob", "jones", 20, "irish", "bob@jones.com", "secret");
    bob.save();
    
    User testUser = User.findByEmail("bob@jones.com");
    assertNotNull (testUser);
  }
  
  @Test
  public void testFindUser()
  {
    User jim = new User("jim", "smith", 20, "irish", "jim@smith.com", "secret");
    jim.save();
    
    User test = User.findByEmail("jim@smith.com");
    assertNotNull (test);

    User alice = User.findByEmail("alice@jones.com");
    assertNull (alice);
  }  
}
~~~

and a MessageTest class:

~~~java
import org.junit.*;

import java.util.*;

import play.test.*;
import models.*;

public class MessageTest extends UnitTest
{
  @BeforeClass
  public static void loadDB()
  {
    Fixtures.deleteAllModels();
  } 
  
  @Test
  public void testCreateMessage()
  {
    User mary = new User("mary", "colllins", 20, "irish", "mary@collins.com", "secret");
    mary.save();
    
    User joan = new User("joan", "colllins", 20, "irish", "joan@collins.com", "secret");
    joan.save();
    
    Message msg = new Message (mary, joan, "Hi there - how are you");
    msg.save();
    
    List<Message> joansMessages = Message.find("byTo", joan).fetch();
    assertEquals (joansMessages.size(), 1);
  }
  
  @Test
  public void testNoMessagese()
  {
    User mary = new User("mary", "colllins", 20, "irish", "mary@collins.com", "secret");
    mary.save();
    
    List<Message> joansMessages = Message.find("byTo", mary).fetch();
    assertEquals (joansMessages.size(), 0);
  }
  
  @Test
  public void testMultipleMessages()
  {
    User mary = new User("mary", "colllins", 20, "irish", "mary@collins.com", "secret");
    mary.save();
    
    User joan = new User("joan", "colllins", 20, "irish", "joan@collins.com", "secret");
    joan.save();
    
    Message msg1 = new Message (mary, joan, "Hi there - how are you");
    msg1.save();
    Message msg2 = new Message (mary, joan, "Where are you now?");
    msg2.save();
    
    List<Message> joansMessages = Message.find("byTo", joan).fetch();
    assertEquals (joansMessages.size(), 2);
    Message message1 = joansMessages.get(0);
    assertEquals(message1.messageText, "Hi there - how are you");
    Message message2 = joansMessages.get(1);
    assertEquals(message2.messageText, "Where are you now?");   
  }
  
}
~~~

Each of these classes is just focused primarily on one of the model classes. You can delete the BasicTest, so your project should look like this:

![](../img/08.png)

And refreshing the test runner site:

![](../img/09.png)

Finally, if we select and run the tests - we can see an individual breakdown of each test:

![](../img/10.png)

Just to confirm that everything is going ok - deliberately break one of the test to see the type of report:

![](../img/11.png)

Revert to 'green state' again by making all tests work






