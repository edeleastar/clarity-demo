#Exercises

The witpress-v2 app, completed during the summer school, implements a simple blogging app with comments. You may have this app in your workspace. If not, this is the archive here:

- [witpress-v2.zip](../archives/witpress-v2.zip)

##Exercise 1:

Your task is to implement a comprehensive set of unit tests to fully exercise the model classes. The following screen capture is and example of the type of tests you should attempt to implement:

![](../img/12.png)

You should have sufficient guidance from this lab to complete these tests. Note that the test names indicate approximately the focus on each of the tests.

The PostTests are a little different in that they exercise features form the collection library - rather than the model classes themselves. I.e. they represent tests that are focused not so much on your classes, but on verifying your understanding of how some library classes work.

##Exercise 2:

Currently the relationships between User and Post, and Post and Comments are navigable in a single direction. Make both of these navigable in both directions. This can be achieved by introducing a many to one relationship to match toe one to many.

Verify that this addition does not break any existing tests, and write some new tests to verify that the relationships are correctly implemented.

##Exercise 3:

Currently a Blog is not modeled - a blog just a collection of posts. Model a Blog as a first class object, developing tests to verify the implementation as you go. Do this in two versions:

- Each user can have a single blog.
- Each user may have zero or more blogs

Dont worry about the UI, just focus on the tests