#More User Tests

You may be surprised that, even if we are running in test mode, the 'admin' interface is still visible:

- <http://localhost:9000/@db>

![](../img/06.png)

Explore the user objects now. You may notice that we seem to have more than one? If you look at the objects data - it appears that the same object 'bob' may be there several times.

This is clearly an anomaly, and is the result of running the tests repeatedly - and having leftover values as a result. This should be corrected immediately as it is very important that the tests are not influenced by whatever may be in the database.

Introduce the following method into BasicTest:

~~~java
  @BeforeClass
  public static void loadDB()
  {
    Fixtures.deleteAllModels();
  }
~~~

Now run the tests a few times - and keep an eye on the number of Users via the admin interface. There should always be only one.

Introduce the following test:

~~~java
  @Test
  public void testFindUser()
  {
    User jim = new User("jim", "smith", 20, "irish", "jim@smith.com", "secret");
    jim.save();
    
    User test = User.findByEmail("jim@smith.com");
    assertNotNull (test);

    User alice = User.findByEmail("alice@jones.com");
    assertNull (alice);
  }  
~~~

This is an example of a 'negative' test. We are deliberately looking for a user (alice) who we know to be not there. Run it and make sure it passes.

Try to make the test deliberately fail?

