#Objectives

- Explore the Unit test features of Play
- Write some tests to verify the current behavior of the User and Message classes for the spacebook app.
- In the exercises, develop a set of unit tests for the wit-press blog.