#Message Tests

We have another model object called Messages already defined in our project:

~~~java
public class Message extends Model
{
  public String messageText;

  @ManyToOne
  public User from;

  @ManyToOne
  public User to;

  public Message(User from, User to, String messageText)
  {
    this.from = from;
    this.to = to;
    this.messageText = messageText;
  }
}
~~~

We can write some tests to see if this model is set up correctly:

~~~java
  @Test
  public void testCreateMessage()
  {
    User mary = new User("mary", "colllins", 20, "irish", "mary@collins.com", "secret");
    mary.save();
    
    User joan = new User("joan", "colllins", 20, "irish", "joan@collins.com", "secret");
    joan.save();
    
    Message msg = new Message (mary, joan, "Hi there - how are you");
    msg.save();
    
    List<Message> joansMessages = Message.find("byTo", joan).fetch();
    assertEquals (joansMessages.size(), 1);
  }
~~~

Look carefully at the above test. It is structured as follows:

- Create a mary user object, and save it to the database
- Create a joan user object, and save it to the database
- Create a message object for a message from mary to joan
- Retrieve all of the messages for joan
- See if there is exactly one message

Run this test now and see if it passes. We can extend the test to see if we also get back the correct text. Put these two lines at the end of the test:

~~~java
    Message message = joansMessages.get(0);
    assertEquals(message.messageText, "Hi there - how are you");
~~~

Run it again and confirm it still passes. Just to confirm that everything is operating ok with the test harness, change a single character in the message, and make sure the test fails. Then change it back so that is passes again.

To conclude, we bring in some more tests to:

- test for the case when there are no message for a given user
- test for the case when there are more than one message for a given user

Here are the tests - bring these directly into your class:

~~~java
  @Test
  public void testNoMessagese()
  {
    User mary = new User("mary", "colllins", 20, "irish", "mary@collins.com", "secret");
    mary.save();
    
    List<Message> joansMessages = Message.find("byTo", mary).fetch();
    assertEquals (joansMessages.size(), 0);
  }
  
  @Test
  public void testMultipleMessages()
  {
    User mary = new User("mary", "colllins", 20, "irish", "mary@collins.com", "secret");
    mary.save();
    
    User joan = new User("joan", "colllins", 20, "irish", "joan@collins.com", "secret");
    joan.save();
    
    Message msg1 = new Message (mary, joan, "Hi there - how are you");
    msg1.save();
    Message msg2 = new Message (mary, joan, "Where are you now?");
    msg2.save();
    
    List<Message> joansMessages = Message.find("byTo", joan).fetch();
    assertEquals (joansMessages.size(), 2);
    Message message1 = joansMessages.get(0);
    assertEquals(message1.messageText, "Hi there - how are you");
    Message message2 = joansMessages.get(1);
    assertEquals(message2.messageText, "Where are you now?");   
  }
~~~


Read the tests and absorb what they are testing. These tests should all pass
