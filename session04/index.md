
- Explore JPA Architecture in more depth
- Understand the implications of JPA annotations on Play model classes
- Review the scope of the CA for this module
