#New Player Feature

Create the following new methods in PlayersController:

~~~java
  public static void newPlayer()
  {
    List<Club> clubs = Club.findAll();
    render(clubs);
  }
  
  public static void createPlayer(String name, String club)
  {
    Logger.info("Name: " + name + ": Club: " + club);
    
    Player player = new Player (name);
    Club theClub = Club.findByName(club);
    if (theClub != null)
    {
      theClub.addPlayer(player);
      theClub.save();
    }
    else
    {
      player.save();
    }
    index();
~~~

These imports will be needed:

~~~java
import java.util.List;
import models.Club;
import play.Logger;
~~~


In PlayersController/index.html, add this at the end inside the closing `<div>`

~~~html
      </div> 
      <a class="btn btn-primary" href="@{PlayersController.newPlayer()}">New Player</a> 
    </div>
~~~

Now create a new html file in the views/PlayersController folder called 'newplayer.html'

~~~html
#{extends 'main.html' /}
#{set title:'Add Player' /}

#{include 'nav/open.html' /}

#{include 'nav/close.html' /}


<div class="container">
  <div class="page-header">
    <h1>Create new Player</h1>
  </div>
  <div class="alert alert-info hidden-phone">
    <a class="close" data-dismiss="alert">x</a>
    <b>Enter name and select club of new player</b> 
  </div>
  
  <form action="@{PlayersController.createPlayer()}" method="POST">
 
  <div class="well">
    <div class="row">
      <div class="span4">
        <div class="control-group">
          <label class="control-label">Player name</label>
          <div class="controls">
            <input name="name" type="text" class="input-medium input-block-level"> 
          </div>
        </div>
      </div>
      <div class="span7">
        <div class="control-group">
          <label class="control-label">Club</label>
          <div class="controls">
            <select name="club">
              #{list items:clubs, as:'club'}
                <option>${club.name}</option>
              #{/list}
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="span2">     
    <button class="btn btn-block">Save</button> 
  </div>
  
  </form>
  
  <div class="span2">
    <a href="@{PlayersController.index()}" class="btn btn-block">Cancel</a> 
  </div>
    
</div>
~~~


Save everything - and verify that you can now add players using the app.

Commit all changes with the message 'New Player Feature'


