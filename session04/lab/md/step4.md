#Test Data + Formatting

There is a data.yaml file in your test folder. Copy this to the conf folder (there will now me two files of this name).

Create a class called 'BootStrap.java' in you 'app' folder, and replace its content with this source here:

##BootStrap.java

~~~java
import models.Division;
import play.jobs.*;
import play.test.*;
 
@OnApplicationStart
public class Bootstrap extends Job<Object> 
{ 
  public void doJob()
  {
    if (Division.count() == 0)
    {
      Fixtures.loadModels("data.yml");
    }
  }
}
~~~

Run the app again, and you should start to see some data.

The views are constructed expecting to have twitter bootstrap loaded. We will do this now.

Replace your 'views/main.html' with the following:

~~~html
<!DOCTYPE html>
  <html>
    <head>
    <title>#{get 'title' /}</title>
    <meta charset="${_response_encoding}">
    <link rel="stylesheet" href="@{'/public/bootstrap/css/bootstrap.min.css'}">
    <script src="@{'/public/javascripts/jquery-1.6.4.min.js'}"></script>
    <script src="@{'/public/bootstrap/js/bootstrap.min.js'}"></script>
    <link rel="shortcut icon" type="image/png" href="@{'/public/images/favicon.png'}">
    <style>
      body 
      {
        padding-top: 60px;
      }
    </style>
  </head>
  <body>
    #{doLayout /}
  </body>
</html>
~~~

Note that we need the bootstrap files in public for this to work. Locate these files and copy them in. Bootstrap has been updated to version 3 since you may have used it last. Locate the v3 version from the web and incorporate.

Reload the app - you should have the menus/views as expected.

Commit this with the message 'Test Data + bootstrap files'
