#Showing Division/Club/Player Relationships

Currently UI for the model is read only - and we can only see each model in isolation (we cant see relationships). We would like to make the relationships visible.

Replace the `<table>` element of the following views with this version:

##views/PlayersController/index.html

~~~html
           <table class="table">
              <thead>
                <tr>
                  <th>Player</th>
                  <th>Club  </th>
                </tr>
              </thead>
              <tbody>
              #{list items:players, as:'player'}
                <tr>
                  <td>${player.name}</td>
                  <td>${player.club.name}</td>
                </tr>
              #{/list}
              </tbody>
            </table>
~~~

##views/ClubsController/index.html

~~~html
            <table class="table">
              <thead>
                <tr>
                  <th>Club</th>
                </tr>
              </thead>
              <tbody>
              #{list items:clubs, as:'club'}
                <tr>
                  <td>${club.name}</td>
                  <td>
                    <table class="table">
                      <tr>
                      #{list items:club.players, as:'player'}
                        <td>${player.name}</td> </tr>
                      #{/list}
                      </tr>
                    </table>
                  </td>                  
                </tr>
              #{/list}
              </tbody>
            </table>
~~~

##views/DivisionsController/index.html

~~~html
            <table class="table">
              <thead>
                <tr>
                  <th>Division</th>
                  <th>Club </th>
                </tr>
              </thead>
              <tbody>
                #{list items:divisions, as:'division'}
                <tr>
                  <td>${division.name}</td>
                  <td>
                    <table class="table">
                      <tr>
                      #{list items:division.members, as:'club'}
                        <td>${club.name}</td> </tr>
                      #{/list}
                      </tr>
                    </table>
                  </td>
                </tr>
                #{/list}
              </tbody
~~~

See if you can make sense of the above templates. Save everything and reload. You should be able to see what players belong to which clubs, and also what divisions the clubs are in.

Commit these changes with a message 'show Division/Club/Player relationships'
