#Edit Player Feature

Introduce the following two methods into the PlayersController:

~~~java
  public static void changePlayer(Long id, String name, Long club)
  {
    Player player = Player.findById(id);
    if (player != null)
    {
      player.name = name;
      Club theClub = Club.findById(club);
      player.club = theClub;
      player.save();
    }
    index();
  }
  
  public static void editPlayer(Long id)
  {
    Player player = Player.findById(id);
    List<Club> clubs = Club.findAll();
    Integer clubIndex = clubs.indexOf(player.club);
    clubIndex++;
    render(player, clubs, clubIndex);
  }
~~~

Incorporate the following new view in views/PlayersController/editplayer.html

~~~html
#{extends 'main.html' /}
#{set title:'Add Player' /}

#{include 'nav/open.html' /}

#{include 'nav/close.html' /}


<div class="container">
  <div class="page-header">
    <h1>Edit Player</h1>
  </div>
  <div class="alert alert-info hidden-phone">
    <a class="close" data-dismiss="alert">x</a>
    <b>Change Player name or Club affiliation</b> 
  </div>
  
  
  #{form @PlayersController.changePlayer(player.id), method:'post'}
  
    <div class="well">
      <div class="row">
        <div class="span4">
          <div class="control-group">
            <label class="control-label">Player name</label>
            <div class="controls">
              <input name="name" type="text" class="input-medium input-block-level" value="${player.name}"> 
            </div>
          </div>
        </div>
        <div class="span7">
          <div class="control-group">
            <label class="control-label">Club</label>
            <div class="controls">
              #{select 'club', items:clubs, value:clubIndex, valueProperty:'id'}
              #{/select} 
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="span2">     
      <button class="btn btn-block">Save</button> 
    </div>
  
  #{/form}
  
  <div class="span2">
    <a href="@{PlayersController.index()}" class="btn btn-block">Cancel</a> 
  </div>
    
</div>
~~~

In views/PlayersController/index.html add a new 'edit' button after the 'delete' button:

~~~html
                 <td> <a class="btn btn-info btn-mini" href="@{PlayersController.editPlayer(player.id)}">Edit</a> </td>
~~~

You should now be able to edit the and change player details.

Commit these changes as "Edit Player Feature"

