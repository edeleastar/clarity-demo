#Create and Delete Clubs

A new version of ClubsController which has several new methods:

~~~java
package controllers;

import java.util.List;

import models.Club;
import models.Division;
import play.Logger;
import play.mvc.Controller;

public class ClubsController extends Controller
{      
  public static void index()
  {
    List<Club> clubs = Club.findAll();
    render (clubs);
  }
  
  public static void newClub()
  {
    List<Division> divisions = Division.findAll();
    render(divisions);
  }
  
  
  public static void delete(Long id)
  {
    Club club = Club.findById(id);
    if (club != null)
    {
      Logger.info("Trying to delete " + club.name);
      List<Division> divisions = Division.findAll();
      for (Division division : divisions)
      {
        if (division.members.contains(club))
        {
          division.members.remove(club);
          division.save();
          Logger.info ("removing club from division");
        }
      }
      club.delete();
    }
    index();
  }  
  
  public static void createClub (String name, String division)
  {    
    Logger.info("name: " + name + "Division: " + division);
    
    Club club = new Club(name);
    club.save();
    
    Division theDivision = Division.findByName(division);
    if (theDivision != null)
    {
      theDivision.addClub(club);
      theDivision.save();
    }
    index();
  }
}
~~~~

A new version of the ClubsController/index.html:

~~~html
#{extends 'main.html' /}
#{set title:'Home' /}

#{include 'nav/open.html' /}
              <li>                <a href="/divisions">     Divisions </a></li>
              <li class="active"> <a href="/clubs">         Clubs     </a></li>
              <li>                <a href="/players">       Players   </a></li>                 
              <li>                <a href="/sponsors">      Sponsors  </a></li>
#{include 'nav/close.html' /}

    <div class="container">
      <div class="page-header">
        <h1>Clubs</h1>
      </div>
      <div class="row">
        <div class="span8">
          <div class="well">
            <table class="table">
              <thead>
                <tr>
                  <th>Club</th>
                  <th>Players</th>
                </tr>
              </thead>
              <tbody>
              #{list items:clubs, as:'club'}
                <tr>
                  <td>${club.name}</td>
                  <td>
                    <table class="table">
                      <tr>
                      #{list items:club.players, as:'player'}
                        <td>${player.name}</td> </tr>
                      #{/list}
                      </tr>
                    </table>
                  </td>    
                  <td> <a class="btn btn-danger btn-mini" href="@{ClubsController.delete(club.id)}">Delete</a> </td>
                </tr>
              #{/list}
              </tbody>
            </table>
          </div>
        </div>
      </div> 
      <a class="btn btn-primary" href="@{ClubsController.newClub()}">New Club</a> 
    </div>
~~~

and a new view - ClubsController/newclub.html:

~~~html
#{extends 'main.html' /}
#{set title:'Add Player' /}

#{include 'nav/open.html' /}

#{include 'nav/close.html' /}


<div class="container">
  <div class="page-header">
    <h1>Create new Club</h1>
  </div>
  <div class="alert alert-info hidden-phone">
    <a class="close" data-dismiss="alert">x</a>
    <b>Enter name of club</b> 
  </div>
  
  <form action="@{ClubsController.createClub()}" method="POST">
  
  <div class="well">
    <div class="row">
      <div class="span4">
        <div class="control-group">
          <label class="control-label">Club name</label>
          <div class="controls">
            <input name="name" type="text" class="input-medium input-block-level"> 
          </div>
        </div>
      </div>
      <div class="span7">
        <div class="control-group">
          <label class="control-label">Division</label>
          <div class="controls">
            <select name="division">
              #{list items:divisions, as:'division'}
                <option>${division.name}</option>
              #{/list}
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="span2">     
    <button class="btn btn-block">Save</button> 
  </div>
  
  </form>
  
  <div class="span2">
    <a href="@{ClubsController.index()}" class="btn btn-block">Cancel</a> 
  </div>
    
</div>
~~~


Verify that you can now add a club using the app.

Commit these changes with the message: "Create and Delete Clubs feature"