#Delete Player Feature

Introduce the following method into the controllers/PlayersController:

~~~java
  public static void delete(Long id)
  {
    Player player = Player.findById(id);
    if (player != null)
    {
      if (player.club != null)
      {
        player.club.removePlayer(player);
        player.club.save();
      }
      player.delete();
    }
    index();
  }
~~~

Which requires this method in models/Club.java

~~~java
  public void removePlayer(Player player)
  {
    players.remove(player);
  }
~~~

Now change the loop in the 'views/PlayersController/index.html' file:

~~~html
              #{list items:players, as:'player'}
                <tr>
                  <td>${player.name}</td>
                  <td>${player.club.name}</td>
                  <td> <a class="btn btn-danger btn-mini" href="@{PlayersController.delete(player.id)}">Delete</a> </td>
                </tr>
              #{/list}
~~~

(just one line changed in the above).

Run the app - players now have a delete button, which should work as expected.

Commit this modification with a 'Delete Player Feature' commit message.