#Controllers/View

Incorporate all of these new artifacts:

##PlayersController

~~~java
package controllers;

import java.util.List;

import models.Player;
import play.mvc.Controller;

public class PlayersController extends Controller
{      
  public static void index()
  {
    List<Player> players = Player.findAll();
    render (players);
  }
}
~~~


##ClubsController

~~~java
package controllers;

import java.util.List;

import models.Club;
import play.mvc.Controller;

public class ClubsController extends Controller
{      
  public static void index()
  {
    List<Club> clubs = Club.findAll();
    render (clubs);
  }
}
~~~

##SponsorsController

~~~java
package controllers;

import java.util.List;

import models.Sponsor;
import play.mvc.Controller;

public class SponsorsController extends Controller
{      
  public static void index()
  {
    List<Sponsor> sponsors = Sponsor.findAll();
    render (sponsors);
  }
}
~~~

##DivisionController

~~~java
package controllers;

import java.util.List;

import models.Division;
import play.mvc.Controller;

public class DivisionController extends Controller
{      
  public static void index()
  {
    List<Division> divisions = Division.findAll();
     
    render (divisions);
  }
}
~~~

You will also need the corresponding views:


##views/PlayersController

~~~html
#{extends 'main.html' /}
#{set title:'Home' /}

#{include 'nav/open.html' /}
              <li>                <a href="/divisions">     Divisions </a></li>
              <li>                <a href="/clubs">         Clubs     </a></li>
              <li class="active"> <a href="/players">       Players   </a></li>                 
              <li>                <a href="/sponsors">      Sponsors  </a></li>
#{include 'nav/close.html' /}

    <div class="container">
      <div class="page-header">
        <h1>Players</h1>
      </div>
      <div class="row">
        <div class="span8">
          <div class="well">
            <table class="table">
              <thead>
                <tr>
                  <th>Player</th>
                  <th>
                  </th>
                  <th>
                  </th>
                </tr>
              </thead>
              <tbody>
              #{list items:players, as:'player'}
                <tr>
                  <td>${player.name}</td>
                  <td></td>
                  <td></td>
                </tr>
              #{/list}
              </tbody>
            </table>
          </div>
        </div>
      </div> 
    </div>

~~~

##views/ClubsController

~~~html
#{extends 'main.html' /}
#{set title:'Home' /}

#{include 'nav/open.html' /}
              <li>                <a href="/divisions">     Divisions </a></li>
              <li class="active"> <a href="/clubs">         Clubs     </a></li>
              <li>                <a href="/players">       Players   </a></li>                 
              <li>                <a href="/sponsors">      Sponsors  </a></li>
#{include 'nav/close.html' /}

    <div class="container">
      <div class="page-header">
        <h1>Clubs</h1>
      </div>
      <div class="row">
        <div class="span8">
          <div class="well">
            <table class="table">
              <thead>
                <tr>
                  <th>Club</th>
                  <th>
                  </th>
                  <th>
                  </th>
                </tr>
              </thead>
              <tbody>
              #{list items:clubs, as:'club'}
                <tr>
                  <td>${club.name}</td>
                  <td></td>
                  <td></td>
                </tr>
              #{/list}
              </tbody>
            </table>
          </div>
        </div>
      </div> 
    </div>
~~~

##views/SponsorsController

~~~html
#{extends 'main.html' /}
#{set title:'Home' /}

#{include 'nav/open.html' /}
              <li>                <a href="/divisions">     Divisions </a></li>
              <li>                <a href="/clubs">         Clubs     </a></li>
              <li>                <a href="/players">       Players   </a></li>                 
              <li class="active"> <a href="/sponsors">      Sponsors  </a></li>
#{include 'nav/close.html' /}

    <div class="container">
      <div class="page-header">
        <h1>Sponsors</h1>
      </div>
      <div class="row">
        <div class="span8">
          <div class="well">
            <table class="table">
              <thead>
                <tr>
                  <th>Sponsor</th>
                  <th>
                  </th>
                  <th>
                  </th>
                </tr>
              </thead>
              <tbody>
              #{list items:sponsors, as:'sponsor'}
                <tr>
                  <td>${sponsor.name}</td>
                  <td></td>
                  <td></td>
                </tr>
              #{/list}
              </tbody>
            </table>
          </div>
        </div>
      </div> 
    </div>

~~~

##views/DivisionsController

~~~html
#{extends 'main.html' /}
#{set title:'Home' /}

#{include 'nav/open.html' /}
              <li class="active"> <a href="/divisions">     Divisions </a></li>
              <li>                <a href="/clubs">         Clubs     </a></li>
              <li>                <a href="/players">       Players   </a></li>                 
              <li>                <a href="/sponsors">      Sponsors  </a></li>
#{include 'nav/close.html' /}

    <div class="container">
      <div class="page-header">
        <h1>Divisions</h1>
      </div>
      <div class="row">
        <div class="span8">
          <div class="well">
            <table class="table">
              <thead>
                <tr>
                  <th>Division</th>
                  <th>
                  </th>
                  <th>
                  </th>
                </tr>
              </thead>
              <tbody>
              #{list items:divisions, as:'division'}
                <tr>
                  <td>${division.name}</td>
                  <td></td>
                  <td></td>
                </tr>
              #{/list}
              </tbody>
            </table>
          </div>
        </div>
      </div> 
    </div>
~~~

These templates need a folder called 'nav' to be created in 'views' - and it must contain:

##open.html

~~~html
<nav class="navbar navbar-default" role="navigation">
  <div class="collapse navbar-collapse">
    <ul class="nav navbar-nav">
~~~

##close.html

~~~html
    </ul>
  </div>
</nav>
~~~

Now incorporate the following routes:

~~~
# Home page
GET     /                                       DivisionController.index
GET     /divisions                              DivisionController.index
GET     /clubs                                  ClubsController.index
GET     /players                                PlayersController.index
GET     /sponsors                               SponsorsController.index
~~~

In addition, as we are powering up the ui, we will need to make sure data source in application.conf is configured:

~~~
db=mem
~~~

Save everything and run (not in test mode). Browse to

- <http://localhost:9000/players>
- <http://localhost:9000/clubs>
- <http://localhost:9000/sponsors>
- <http://localhost:9000/divisions>

It views will be largely bank and unformatted. 

Commit these changes, nevertheless, using the string "Controllers/Views for all models"

