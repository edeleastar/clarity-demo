#Exercises

##Solution

Completed version:

- [schema-final.zip](../archives/schema-final.zip)

##Exploring the Database

Edit the file ComprehensiveTest.java test case, and comment our all of the @Test annotations except one (say the first one). This will remove all tests from the test runner. 

Also, comment out the call to 'deleteAllModels()' from tearDown()

~~~java
  @After
  public void teardown()
  {
    //Fixtures.deleteAllModels();
  }
~~~

Restart the app in test mode, and run just the comprehensive test (not the others).

While still in test mode, browse to:

- <http://localhost:9000>
- <http://localhost:9000/@db>

You should be able to see a fully populate UI + Database. You may be interested in looking at each of the tables in turn on the database to get a feel for the mapping performed by JPA. A useful exercise would be to use pen & paper to draw the all the tables and their contents. Use ER mapping notation to show the table relationships. This could prove invaluable in debugging relationships you may be designing for your assignments.

Be sure to comment the test annotation back in when you finish.


##Divisions & Sponsors

The divisions and sponsors controllers/view are currently read-only. I.ee it is not possible to add or edit the entities. Using the Players and CLubs as examples, bring in the ability create/edit divisions and sponsors.

##New Bootstrap template site

If you have made the switch to bootstrap 3 - then you might be interested in this new site with free bootstrap templates here:

- <http://startbootstrap.com/>

It includes some very useful Full Site Templates:

- <http://startbootstrap.com/full-site-templates>

See this demo here:

- <http://startbootstrap.com/templates/modern-business>

In particular, try the last link on a mobile device. If you dont have one connected, browse to that site using the Android Simulator.

