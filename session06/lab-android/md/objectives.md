#Objectives

- Layout a new Android project with a single activity
- Design this activity to permit donations to be made using simple controls
- Implement the Activity class to support these controls