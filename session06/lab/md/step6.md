#Creating coffee objects

We can read individual coffee objects, or a collection of coffees. How about Creating a coffee object?

In order to do this we will need an additional utility class in our coffemate-service Play project. In the utils package, import this class here:

~~~java
package utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import play.data.binding.Global;
import play.data.binding.TypeBinder;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

@Global
public class GsonBinder implements TypeBinder<JsonElement>
{
  public Object bind(String name, Annotation[] notes, String value, Class toClass, Type toType) throws Exception
  {
    return new JsonParser().parse(value);
  }
}
~~~

The purpose of this class is rather opaque. Its task is to help deliver a Json payload to our controllers. Dont worry about how this is done - we will never need to change this class - so we can merely include it as is.

Introduce this new route here:

~~~
POST    /api/coffees                           CoffeeServiceAPI.createCoffee
~~~

and the method in the controller to handle this route:

~~~java
  public static void createCoffee(JsonElement body)
  {
    Coffee coffee = JsonParsers.json2Coffee(body.toString());
    coffee.save();
    renderJSON (JsonParsers.coffee2Json(coffee));
  } 
~~~

You will need to import JsonElement at the top of the file:

~~~
import com.google.gson.JsonElement
~~~

Before writing a test - which would be the logical next step - we can use Postman to manually drive the api and generate the appropriate http request.

This time it is a bit more complicated however - as we are generating a POST request, not a single GET like last time. In addition, we need to include a payload, which is to contain a json version of a coffee object.

First we need to specify 2 headers:

~~~
Content-Type: application/json
~~~

These are specified using the "Header" button on the right. If you figure it out, it should look like this:

![](../img/08.png)

Then we need to specify the payload - also called 'form data':

![](../img/09.png)

In the above, we selected 'raw' and 'json' and entered the following data:

~~~javascript
{
  "name" : "mocha",
  "shop" : "costa"
}
~~~

Note carefully the ',' separating the two values. Before running the coffeemate-service, disable the loading for data.yaml first:

~~~java
@OnApplicationStart
public class Bootstrap extends Job 
{ 
  public void doJob()
  {
   //Fixtures.loadModels("data.yml");
  }
}
~~~

Run the app again, and press 'Send' in Postman. If all goes correctly, then coffeemate-service should respond with a new coffeemate object:

![](../img/10.png)

See if you can verify that this has in fact been created in the database.

Create a few more coffee objects - give them different names and process. After each one is sent, check the database for these new values.

Notice that you can 'save' requests in postman. This allows you to construct different requests and run them again later. They appear in the list on the left in postman.




