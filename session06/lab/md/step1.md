#Create Test Project

In Eclipse, create a new standard Java project called 'coffeemate-test'. 

The following archive contains a set of jar files to be used by this project:

- [jar-archives.zip](../archives/jar-archives.zip)

Download and unzip this archive somewhere convenient.

In Eclipse, create a new 'folder' called lib (not a 'source folder') in your project, and drag and drop (copy) all of the jar files into that folder.

Still in eclipse, select all of the jar files, right click, and select 'add to build path'. This will place all of these jars on the path for the project.

In addition to the above libraries, also add JUnit 4 to your project. This can be done through Project->Properties->Build Path->Libraries->Add Library.

Your project should now look like this:

![](../img/00.png)

These are a set of libraries which will greatly simplify the task of testing our API. They include the following:

- <http://hc.apache.org>
- <http://hc.apache.org/httpcomponents-client-ga/tutorial/html/fluent.html>
- <https://code.google.com/p/guava-libraries/wiki/GuavaExplained>

plus other libraries that these components rely on.