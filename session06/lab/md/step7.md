#Testing Create Coffee 

Now that we have proved we can create coffee objects using POST - we should write a test in Java that does just that.

Returning to the coffeemate-test project, we need a new method in the Rest class:

~~~java
  public static String post(String path, String json) throws Exception
  {
    HttpPost putRequest = new HttpPost(URL + path);
    putRequest.setHeader("Content-type", "application/json");
    putRequest.setHeader("accept", "application/json");
    
    StringEntity s = new StringEntity(json);
    s.setContentEncoding("UTF-8");
    s.setContentType("application/json");
    putRequest.setEntity(s);

    HttpResponse response = httpClient().execute(putRequest);
    return new BasicResponseHandler().handleResponse(response);
  }
~~~

This will be used to formulate a POST request.

In CoffeeAPI class (in the coffeemate-test project), introduce these two new methods:

~~~java
  public static Coffee createCoffee(String coffeeJson) throws Exception
  {
    String response = Rest.post ("/api/coffees", coffeeJson);
    return JsonParsers.json2Coffee(response);
  }
  
  public static Coffee createCoffee(Coffee coffee) throws Exception
  {
    return createCoffee(JsonParsers.coffee2Json(coffee));
  }
~~~

These provide a higher level mechanism for creating coffee objects using the API.

Now, in CoffeeTest, we can write an elegant test:

~~~java
  @Test
  public void testCreateCoffee () throws Exception
  {
    Coffee coffee =  new Coffee ("cappucino", "starbucks", 1.5, 4.0, 0);
    Coffee returnedCoffee = CoffeeAPI.createCoffee(coffee);
    
    assertEquals (coffee, returnedCoffee);
  }
~~~

Our existing test should be commented out now, as it is no longer reliable.
Run this new test - it should pass. 

Inspect the database - you should see the 'cappucino' created. If you run the test a few times, then you will get multiple cappuccino coffees created.


