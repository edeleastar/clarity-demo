#Coffeemate Facade

Create two packages in the src folder called:

- ie.cm.api
- test

In ie.cm.api, create a new class called Rest, this is the complete source for this class:

~~~java
package ie.cm.api;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public class Rest
{
  private static DefaultHttpClient httpClient = null;
  private static final String URL = "http://localhost:9000";

  private static DefaultHttpClient httpClient()
  {
    if (httpClient == null)
    {
      HttpParams httpParameters = new BasicHttpParams();
      HttpConnectionParams.setConnectionTimeout(httpParameters, 2000);
      HttpConnectionParams.setSoTimeout(httpParameters, 2000);
      httpClient = new DefaultHttpClient(httpParameters);
    }
    return httpClient;
  }

  public static String get(String path) throws Exception
  {
    HttpGet getRequest = new HttpGet(URL + path);
    getRequest.setHeader("accept", "application/json");
    HttpResponse response = httpClient().execute(getRequest);
    return new BasicResponseHandler().handleResponse(response);
  }
}
~~~

Still in ie.cm.api, create the following three classes:

##Coffee

~~~java
package ie.cm.api;

import com.google.common.base.Objects;

public class Coffee
{
  public Long   id;
  public String name;
  public String shop;
  public double rating;
  public double price;
  public int    favourite;

  public Coffee()
  {   
  }
  
  public Coffee(String coffeeName, String shop, double rating, double price, int favourite)
  {
    this.name      = coffeeName;
    this.shop      = shop;
    this.rating    = rating;
    this.price     = price;
    this.favourite = favourite;
  }

  @Override
  public boolean equals(final Object obj)
  {
    if (obj instanceof Coffee)
    {
      final Coffee other = (Coffee) obj;
      return Objects.equal(name,      other.name) 
          && Objects.equal(shop,      other.shop)
          && Objects.equal(rating,    other.rating)
          && Objects.equal(price,     other.price)           
          && Objects.equal(favourite, other.favourite);                     
    }
    else
    {
      return false;
    }
  }  
  
  public String toString()
  {
    return Objects.toStringHelper(this)
        .add("id",        id)
        .add("name",      name)
        .add("shop",      shop)
        .add("rating",    rating)
        .add("price",     price)
        .add("favourite", favourite).toString();
  } 
}
~~~

##JsonParsers

~~~java
package ie.cm.api;

import java.lang.reflect.Type;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class JsonParsers
{
  static Gson gson = new Gson(); 
 
  public static Coffee json2Coffee(String json)
  {
    return gson.fromJson(json, Coffee.class);    
  }
  
  public static String coffee2Json(Object obj)
  {
    return gson.toJson(obj);
  }  
  
  public static List<Coffee> json2Coffees(String json)
  {
    Type collectionType = new TypeToken<List<Coffee>>() {}.getType();
    return gson.fromJson(json, collectionType);    
  }  
}
~~~

###CoffeeAPI

~~~java
package ie.cm.api;


public class CoffeeAPI
{

  public static Coffee getCoffee (Long id) throws Exception
  {
    String coffeeStr = Rest.get("/api/coffee/" + id);
    Coffee coffee = JsonParsers.json2Coffee(coffeeStr);
    return coffee;
  }
}
~~~

Note that the first two class are (almost) identical to two of the classes in the companion coffeemate-service play project. Check these similarities now.


