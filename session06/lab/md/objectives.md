#Objectives

- Design a new java project to specifically test the coffeemate api
- Build utility classes to simplify the tests
- Enhance coffemate-service to enable Coffee objects to be created