#First Test

Still in coffeemate-test project, in the test package, bring in this class here:

~~~
package test;

import org.junit.Test;

import coffeeservice.Coffee;
import coffeeservice.CoffeeAPI;

import utils.Rest;

public class CoffeeTest
{
  @Test
  public void testGetCoffee () throws Exception
  {
     String coffeeStr = Rest.get("/api/coffee/" + 1);
     System.out.println(coffeeStr);
  }
}
~~~

Now launch the current version of the coffemate-service play project. In order to make sure the project is properly loaded, browse to the usual web interface:

- <http://localhost:9000/>

Now in the coffeemate-test project, select the CoffeeTest class, right click, and select 'Run as -> JUnit Test'.

This should display the Eclipse JUnit test runner:

![](../img/02.png)

In addition - we should see in the 'Console' view in eclipse, the output of the test we have written above:

![](../img/03.png)

Can you see what it happening here? We are issuing a request - over http - to this url:

<http://localhost:9000/api/coffee/1>

and retrieving the coffee object with ID 1.

Comment out the two lines in the above code - and replace them with the following:

~~~java
    Coffee coffee = CoffeeAPI.getCoffee(2l);
    System.out.println(coffee);
~~~

Run the test again. The result should be almost identical. Uncomment the test such that it looks like this:

~~~java
    String coffeeStr = Rest.get("/api/coffee/" + 1);
    System.out.println(coffeeStr);
    Coffee coffee = CoffeeAPI.getCoffee(2l);
    System.out.println(coffee);
~~~

Running the test again, you should see this:

~~~
{"favourite":0,"id":1,"name":"one","price":2.0,"rating":3.5,"shop":"here"}
Coffee{id=2, name=two, shop=there, rating=4.5, price=3.0, favourite=1}
~~~

What is the difference between the two lines output?