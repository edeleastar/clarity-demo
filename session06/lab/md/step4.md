#Complete CoffeeTest

The test we have written is not actually a test, as it contains no assert statements. It merely prints some data to the console. We should rewrite it such that it does a proper test. i.e. verify that we receive a specific, agreed object from the coffemate-service.

Replace CoffeeTest with the following version:

~~~java
package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import coffeeservice.Coffee;
import coffeeservice.CoffeeAPI;

public class CoffeeTest
{
  private Coffee localCoffee;
  
  @Before
  public void setup()
  {
    localCoffee = new Coffee ("two", "there", 4.5, 3.0, 1);
  }
  
  @After
  public void teardown()
  {
    localCoffee = null; 
  }
  
  @Test
  public void testGetCoffee () throws Exception
  {
    Coffee coffee = CoffeeAPI.getCoffee(2l);
    assertEquals (localCoffee, coffee);
  }
}
~~~

and run it in the same way. This time the JUnit test runner should report success - with a 'green bar' state.

Carefully read the test itself:

~~~java
    Coffee coffee = CoffeeAPI.getCoffee(2l);
    assertEquals (localCoffee, coffee);
~~~

We are requesting a coffee object with id 2, and, when we get it, verifying that it equals another (test) object we have created just for this purpose.

Just to confirm everything is behaving as expected, change the localCoffee object slightly (say setting favorite to 0). Run the test. It should fail. Correct the test to run successfully again.

