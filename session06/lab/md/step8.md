#Sources

The two projects should look something like this:

![](../img/12.png)

![](../img/13.png)

These are the respective archives:

- [coffeemate-service-lab06.zip](../archives/coffeemate-service-lab06.zip)
- [coffeemate-test-lab06.zip](../archives/coffeemate-test-lab06.zip)

Here are (slightly revised) version of the sources:

##coffeemate-service

###Coffee

~~~java
@Entity
public class Coffee extends Model
{
  public String name;
  public String shop;
  public double rating;
  public double price;
  public int    favourite;

  public Coffee(String coffeeName, String shop, double rating, double price, int favourite)
  {
    this.name      = coffeeName;
    this.shop      = shop;
    this.rating    = rating;
    this.price     = price;
    this.favourite = favourite;
  }

  @Override
  public boolean equals(final Object obj)
  {
    if (obj instanceof Coffee)
    {
      final Coffee other = (Coffee) obj;
      return Objects.equal(name,      other.name) 
          && Objects.equal(shop,      other.shop)
          && Objects.equal(rating,    other.rating)
          && Objects.equal(price,     other.price)           
          && Objects.equal(favourite, other.favourite);                     
    }
    else
    {
      return false;
    }
  }  
  
  public String toString()
  {
    return Objects.toStringHelper(this)
        .add("Id", id)
        .add("name",      name)
        .add("shop",      shop)
        .add("rating",    rating)
        .add("price",     price)
        .add("favourite", favourite).toString();
  } 
}
~~~

###CoffeServiceAPI

~~~java
public class CoffeeServiceAPI extends Controller
{
  public static void coffees()
  {
    List<Coffee> coffees = Coffee.findAll();
    renderText(JsonParsers.coffee2Json(coffees));
  }
  
  public static void coffee (Long id)
  {
   Coffee coffee = Coffee.findById(id);
   renderJSON (JsonParsers.coffee2Json(coffee));
  }
  
  public static void createCoffee(JsonElement body)
  {
    Coffee coffee = JsonParsers.json2Coffee(body.toString());
    coffee.save();
    renderJSON (JsonParsers.coffee2Json(coffee));
  }  
}
~~~

###routes

~~~
# Routes
# This file defines all application routes (Higher priority routes first)
# ~~~~

# Home page
GET     /                                      Application.index

GET     /api/coffees                           CoffeeServiceAPI.coffees
GET     /api/coffees/{id}                      CoffeeServiceAPI.coffee
POST    /api/coffees                           CoffeeServiceAPI.createCoffee

# Ignore favicon requests
GET     /favicon.ico                            404

# Map static resources from the /app/public folder to the /public path
GET     /public/                                staticDir:public

# Catch all
*       /{controller}/{action}                  {controller}.{action}

~~~

##coffeemate-test

##Coffee

~~~java
public class Coffee
{
  public Long   id;
  public String name;
  public String shop;
  public double rating;
  public double price;
  public int    favourite;

  public Coffee()
  {   
  }
  
  public Coffee(String coffeeName, String shop, double rating, double price, int favourite)
  {
    this.name      = coffeeName;
    this.shop      = shop;
    this.rating    = rating;
    this.price     = price;
    this.favourite = favourite;
  }

  @Override
  public boolean equals(final Object obj)
  {
    if (obj instanceof Coffee)
    {
      final Coffee other = (Coffee) obj;
      return Objects.equal(name,      other.name) 
          && Objects.equal(shop,      other.shop)
          && Objects.equal(rating,    other.rating)
          && Objects.equal(price,     other.price)           
          && Objects.equal(favourite, other.favourite);                     
    }
    else
    {
      return false;
    }
  }  
  
  public String toString()
  {
    return Objects.toStringHelper(this)
        .add("id",        id)
        .add("name",      name)
        .add("shop",      shop)
        .add("rating",    rating)
        .add("price",     price)
        .add("favourite", favourite).toString();
  } 
}
~~~

###CoffeeAPI

~~~java
public class CoffeeAPI
{  
  public static Coffee getCoffee (Long id) throws Exception
  {
    String coffeeStr = Rest.get("/api/coffees/" + id);
    Coffee coffee = JsonParsers.json2Coffee(coffeeStr);
    return coffee;
  }
  
  public static Coffee createCoffee(String coffeeJson) throws Exception
  {
    String response = Rest.post ("/api/coffees", coffeeJson);
    return JsonParsers.json2Coffee(response);
  }
  
  public static Coffee createCoffee(Coffee coffee) throws Exception
  {
    return createCoffee(JsonParsers.coffee2Json(coffee));
  }

  public static List<Coffee> getCoffees () throws Exception
  {
    String response =  Rest.get("/api/coffees");
    List<Coffee> coffeeList = JsonParsers.json2Coffees(response);
    return coffeeList;
  } 
}
~~~

###Rest

~~~java
public class Rest
{
  private static DefaultHttpClient httpClient = null;
  private static final String URL = "http://localhost:9000";

  private static DefaultHttpClient httpClient()
  {
    if (httpClient == null)
    {
      HttpParams httpParameters = new BasicHttpParams();
      HttpConnectionParams.setConnectionTimeout(httpParameters, 2000);
      HttpConnectionParams.setSoTimeout(httpParameters, 2000);
      httpClient = new DefaultHttpClient(httpParameters);
    }
    return httpClient;
  }

  public static String get(String path) throws Exception
  {
    HttpGet getRequest = new HttpGet(URL + path);
    getRequest.setHeader("accept", "application/json");
    HttpResponse response = httpClient().execute(getRequest);
    return new BasicResponseHandler().handleResponse(response);
  }
  
  public static String post(String path, String json) throws Exception
  {
    HttpPost putRequest = new HttpPost(URL + path);
    putRequest.setHeader("Content-type", "application/json");
    putRequest.setHeader("accept", "application/json");
    
    StringEntity s = new StringEntity(json);
    s.setContentEncoding("UTF-8");
    s.setContentType("application/json");
    putRequest.setEntity(s);

    HttpResponse response = httpClient().execute(putRequest);
    return new BasicResponseHandler().handleResponse(response);
  }
}
~~~

###CoffeeTest

~~~java
public class CoffeeTest
{
  private Coffee mocha;
  private long   mochaId;
  
  @Before
  public void setup() throws Exception
  {
    Coffee returnedCoffee;
    
    mocha     = new Coffee ("mocha",     "costa",  4.5, 3.0, 1);
    returnedCoffee = CoffeeAPI.createCoffee(mocha);
    mochaId = returnedCoffee.id;
  }
  
  @After
  public void teardown()
  {
    mocha     = null; 
  }
  
  @Test
  public void testCreateCoffee () throws Exception
  {
    assertEquals (mocha, CoffeeAPI.getCoffee(mochaId));
  }
}
~~~

This file is shared by both projects:

###JsonParsers

~~~java
public class JsonParsers
{
  static Gson gson = new Gson();
  
  public static String user2Json(Object obj)
  {
    return gson.toJson(obj);
  }  
 
  public static Coffee json2Coffee(String json)
  {
    return gson.fromJson(json, Coffee.class);    
  }
  
  public static String coffee2Json(Object obj)
  {
    return gson.toJson(obj);
  }  
  
  public static List<Coffee> json2Coffees(String json)
  {
    Type collectionType = new TypeToken<List<Coffee>>() {}.getType();
    return gson.fromJson(json, collectionType);    
  }  
}
~~~
