#Improving the Service

Our service interface - implemented in CoffeeServiceAPI is still fairly simple:

~~~java
public class CoffeeServiceAPI extends Controller
{
  public static void getCoffees()
  {
    List<Coffee> coffees = Coffee.findAll();
    renderText(coffees.get(0));
  }
  
  public static void coffee (Long id)
  {
   Coffee coffee = Coffee.findById(id);
   renderJSON (JsonParsers.coffee2Json(coffee));
  }
}
~~~

... and is routed via these entries in the routes file:

~~~
GET     /api/coffees                           CoffeeServiceAPI.getCoffees
GET     /api/coffee/{id}                       CoffeeServiceAPI.coffee
~~~

The second route is adequate - but we delete the first method and route. Our ServiceAPI should look like this:

~~~java
public class CoffeeServiceAPI extends Controller
{
  public static void coffee (Long id)
  {
   Coffee coffee = Coffee.findById(id);
   renderJSON (JsonParsers.coffee2Json(coffee));
  }
}
~~~

.. and our route. Note we have made it plural:

~~~
GET     /api/coffees/{id}                       CoffeeServiceAPI.coffee
~~~

Now introduce the following new route:

~~~
GET     /api/coffees                            CoffeeServiceAPI.coffees
~~~

Look carefully at the change - both routes begin with /api/coffees (note the plural coffeeS), but route to different methods. We already have coffee implemented, here is a first attempt at coffees:

~~~java
  public static void coffees()
  {
    List<Coffee> coffees = Coffee.findAll();
    renderText(JsonParsers.coffee2Json(coffees));
  }
~~~

Run the app again and browse to:

- <http://localhost:9000/api/coffees>

The browser should return:

~~~
[{"favourite":0,"id":1,"name":"one","price":2.0,"rating":3.5,"shop":"here"},{"favourite":1,"id":2,"name":"two","price":3.0,"rating":4.5,"shop":"there"}]
~~~

This is a little hard to read. Chrome has a browser extension that can make this task more effective. You may have installed this last week. If not, on chrome, search for "Postman Rest Client". You may get this link here:

- [Postman](https://chrome.google.com/webstore/detail/postman-rest-client/fdmmgilgnpjigdojojpjoooidkmcomcm?hl=en)

Install the package - and when you launch it you should see something like this:

![](../img/04.png)

Now enter our request `http://localhost:9000/api/coffees` - and we should see this:

![](../img/05.png)

Pressing the Json button presents the data in a more readable format:

![](../img/06.png)

This is a very useful tool for experimentally verifying that our API is behaving as expected.
