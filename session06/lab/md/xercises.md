#Exercises

##Test Coffee create and get

Rework CoffeeTest class to be a more comprehensive test. i.e. it should create a number of coffee objects and, once created, check to see if they exist.

##Delete coffee

How would we go about deleting a coffee? It might have some similarities to the getCoffee route?

##Test coffee list get

Our list coffees api should still be working nicely:

- <http://localhost:9000/api/coffees>

How would we approach writing a test for this?

##Cloudbees

See if you can publish the coffeemate-service to cloudbees. Can you then run the test to use the Cloudbees version instead of local one?

#Solutions

The solutions to the above exercises will be the subject of next weeks lab.

