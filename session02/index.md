
- Introduce UML
- Explore the fundamentals of the 'one to many' relationship including:
    - How it is modelled in UML
    - How it is implemented in JPA
- Furthermore, explore how to model and implementation a bidirectional variant of this relationship

