#Modeling a Player / Club Relationship

Returning to Visual Paradigm - you may notice a red line under each class. It says 'Very bad' if you click on it. Do so now, and in a subsequent dialog you will have an opportunity to disable this 'Quality Checker' completely We will not be using this feature.

On the palette on the left, select the 'association' element and use it to connect Club and Player. It should look like this:

![](../img/10.png)

Select the association (the line), and locate the following panel (which should look like this:

![](../img/11.png)

Now edit as follows:

For Role A:

![](../img/12.png)

For Role B:

![](../img/13.png)

If this works, then the class diagram should look like this:

![](../img/14.png)

We can now implement this relationship. In Eclipse, modify Club as follows:

~~~java
public class Club extends Model
{
  //...	
  @OneToMany(cascade=CascadeType.ALL)
  public List<Player> players;

  public Club(String name)
  {
    this.name = name;
    this.players = new ArrayList<Player>();
  }
  //...
  public void addPlayer(Player player)
  {
    players.add(player);
  }
  //...
}
~~~

This establishes the relationship as modeled in UML.



