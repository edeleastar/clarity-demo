#Initial Model for Player and Club

In your jpamodel eclipse project, create two new classes in the model package:

##Club
~~~java
package models;

import javax.persistence.Entity;
import play.db.jpa.Model;

@Entity
public class Club extends Model
{
  public String name;

  public Club(String name)
  {
    this.name = name;
  }
}
~~~

##Player

~~~java
package models;

import javax.persistence.Entity;
import play.db.jpa.Model;

@Entity
public class Player extends Model
{
  public String name;

  public Player(String name)
  {
    this.name = name;
  }
}
~~~

Now we create the unit test harnesses. In 'test/default package' create these two tests:

##ClubTest

~~~java
import org.junit.*;

import java.util.*;
import play.test.*;
import models.*;

public class ClubTest extends UnitTest 
{
  @Before
  public void setup()
  {
  }
  
  @After
  public void teardown()
  {
  }

  @Test
  public void testCreate()
  {
    
  }
}
~~~

##PlayerTest

~~~java
import org.junit.*;

import java.util.*;
import play.test.*;
import models.*;

public class PlayerTest extends UnitTest 
{
  @Before
  public void setup()
  {
  }
  
  @After
  public void teardown()
  {
  }

  @Test
  public void testCreate()
  {
    
  }
}
~~~

Run the app now in 'test' mode:

~~~
play test
~~~

...and navigate to the test runner page:

- <http://localhost:9000/@tests>

Select the Club and Player tests - and they should be green.

Also try the database interface:

- <http://localhost:9000/@db>


