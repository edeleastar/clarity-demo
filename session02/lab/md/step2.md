#Visual Paradigm

For this step to work you will need to have installed Visual Paradigm. This is the particular product here:

- <http://www.visual-paradigm.com/product/?favor=vpuml>

... and it can be downloaded here

- <http://www.visual-paradigm.com/download/vpuml.jsp>

If you select the 'no install' version, then it can be reliably run from a memory stick if you need to operate that way. Otherwise install as normal.

You will require this Key in order to activate the application:

Key for Visual Paradigm 10.2: Standard Edition: D3IT6-2Q394-3PC2B-SF8GH-3DN44

The pc will need to be online when it app launches in order to validate. This is a subscription license - Academic. When you start the application you will have to locate and paste the license into the appropriate box.

Launch Visual Paradigm now, and select File->Create New Project:

![](../img/01.png)

This should present a screen like this:

![](../img/02.png)

---

We start by creating a new "Class Diagram":

---

![](../img/03.png)

Press OK and the class diagram editor is displayed:

---

Before we create any classes, we are going to make some small adjustments to the default look and feel. Select 'Tools->Projects Options->Diagramming->Shape' and make the 'default shape fill format'  white (instead of blue currently):

---

![](../img/07.png)

---

Then select 'Class->Presentation' and set 'show attribute option' and 'show operation option' to both be 'hide all'

![](../img/08.png)
---

Drag the 'class' icon onto the canvas, call the class Club. Create another one and call it Player. Your canvas should look like this:

---

![](../img/09.png)
---

Save the project. Make sure you remember where you put it. A useful place is to put in inside the same folder as the play project
