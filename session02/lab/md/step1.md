#Play Update + create new project.

The play framework has been updated recently to 1.2.7. Download the new version here:

- <http://www.playframework.com/download>

Recall, installing play requires you do two things:

- expand the play zip file somewhere on your PC
- place the expanded folder on your path.

Unless you do both steps, you will still be using the older version.

Now creating a brand new Play project. Do this by navigating to the correct folder folder (most likely your workspace) and running a command prompt. Then type:

~~~
play new jpamodel
~~~

Once this has completed, change into the folder just created (jpamodel) and run the eclipsify command:

~~~
cd jpamodel
play eclipsify
~~~

You can now import the project into eclipse in the usual way.

Once imported, you may need to make sure the project is configured for an in-memory database - by verifying that conf.ini contains the following:

~~~
db=mem
~~~
