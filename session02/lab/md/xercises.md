#Exercises

This is an extension of the model to include:

- Divisions
- Sponsors

![](../img/18.png)

You should be able to: 

- Replicate this model in Visual Paradigm. 
- Incorporate the new classes into the jpamodel project
- Build some unit tests to exercises the model