#Exercises

The witpress-v2 app, completed during the summer school, implements a simple blogging app with comments. You may have this app in your workspace. If not, this is the archive here:

- [witpress-v2.zip](../archives/witpress-v2.zip)

##Exercise 1:

Your task is to implement a comprehensive set of unit tests to fully exercise the model classes. The following screen capture is and example of the type of tests you should attempt to implement:

![](../img/12a.png)

You should have sufficient guidance from this lab to complete these tests. Note that the test names indicate approximately the focus on each of the tests.

The PostTests are a little different in that they exercise features form the collection library - rather than the model classes themselves. I.e. they represent tests that are focused not so much on your classes, but on verifying your understanding of how some library classes work.


##Exercise 1 Solution:

- [BlogTest](../archives/BlogTest.java)
- [PostTest](../archives/PostTest.java)
- [CommentTest](../archives/CommentTest.java)


##Exercise 3:

Currently a Blog is not modeled - a blog just a collection of posts. Model a Blog as a first class object, developing tests to verify the implementation as you go. Do this in two versions:

- Each user can have a single blog.
- Each user may have zero or more blogs

Dont worry about the UI, just focus on the tests

##Exercise 3 Solution:

This is a complete solution to Exercise 3 :

- [witpress-v3.zip](../archives/witpress-v3.zip)

Which encompasses:

- Each user may have zero or more blogs - mode change
- Tests which verify this model change
- UI, sufficient changes have been made to the UI to enable it to function using the revised model.


