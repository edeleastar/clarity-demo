#Objectives

- Enable menus to support navigation between Donate and Report
- Introduce an Application Object to host list of donations
- Introduce an Adapter to render this list