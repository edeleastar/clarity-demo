#Refactored Report

We now rework Report to render the actual donations - held in the DonationApp list.

First some layout additions. Include these new string in strings.xml

~~~
    <string name="defaultAmount">00</string>
    <string name="defaultMethod">N/A</string>
~~~

This is a new layout - to be called 'row_donate.xml'. Place this in the 'layout' folder.

~~~
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent" >

    <TextView
        android:id="@+id/row_amount"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignParentLeft="true"
        android:layout_alignParentTop="true"
        android:layout_marginLeft="48dp"
        android:layout_marginTop="20dp"
        android:text="@string/defaultAmount" />

    <TextView
        android:id="@+id/row_method"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignBaseline="@+id/row_amount"
        android:layout_alignBottom="@+id/row_amount"
        android:layout_marginLeft="106dp"
        android:layout_toRightOf="@+id/row_amount"
        android:text="@string/defaultMethod" />

</RelativeLayout>
~~~

Finally, rework Report class to remove the hard coded values - and use a different 'adapter'

~~~java
package app.activities;

import java.util.List;
import app.donation.R;
import app.main.DonationApp;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import app.models.Donation;

public class Report extends Activity
{
  private ListView    listView;
  private DonationApp app;

  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_report);

    app = (DonationApp) getApplication();

    listView = (ListView) findViewById(R.id.reportList);
    DonationAdapter adapter = new DonationAdapter (this, app.donations);
    listView.setAdapter(adapter);
  }
}
~~~

This is the new adapter - DonationAdapter. You can place this at the end of the Report class (outside the closing brace) if you like:

~~~java
class DonationAdapter extends ArrayAdapter<Donation>
{
  private Context        context;
  public  List<Donation> donations;

  public DonationAdapter(Context context, List<Donation> donations)
  {
    super(context, R.layout.row_donate, donations);
    this.context   = context;
    this.donations = donations;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent)
  {
    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 
    View     view       = inflater.inflate(R.layout.row_donate, parent, false);
    Donation donation   = donations.get(position);
    TextView amountView = (TextView) view.findViewById(R.id.row_amount);
    TextView methodView = (TextView) view.findViewById(R.id.row_method);
    
    amountView.setText("" + donation.amount);
    methodView.setText(donation.method);
    
    return view;
  }

  @Override
  public int getCount()
  {
    return donations.size();
  }
}
~~~

If all goes well - then you should be able to make donations, and and the see a list of the in the report activity.

