
- Review the structure of Android applications
- Explore Android Menus and the precise layout files required to support them
- Introduce the Application object
- Introduce a simple Adapter to render a custom list